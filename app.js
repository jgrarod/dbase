var ppl = Lawnchair(function() {

    // traditional callback code
    this.save({key:'config', options:[1,2,3]}, function(obj){
        console.log(obj)
    })

    // terse callback style
    this.save({person:'joni'}, 'console.log(record)')
})